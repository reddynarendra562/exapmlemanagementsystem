package ExapmleManagementSystem;


import java.util.Comparator;

public class Employee  {
    int id;
   public String name;
    int salary;
    String department;
    String address;

    public Employee(int id, String name, int salary, String department, String address) {
        this.id = id;
        this.setName(name);
        this.salary = salary;
        this.department = department;
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + getName() + '\'' +
                ", salary=" + salary +
                ", department='" + department + '\'' +
                ", address='" + address + '\'' +
                '}';
    }

}
class CompareName implements Comparator<Employee> {

    @Override
    public int compare(Employee o1, Employee o2) {
     return o1.name.compareTo(o2.name);
    }
}

