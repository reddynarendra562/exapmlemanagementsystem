package ExapmleManagementSystem;


import java.io.*;
import java.util.*;
import java.util.List;

public class EmployeeManagement {
    public static void main(String[] args) throws IOException {
        int id;
        String name;
        int salary;
        String department = null;
        String address;
        Scanner sc = new Scanner(System.in);
        Set<Employee> hs = new HashSet<Employee>();
        do {
            System.out.println("Employee management System");
            System.out.println("What do you want?");
            System.out.println("1.Insert the employee data");
            System.out.println("2.Delete the employee data");
            System.out.println("3.Update the employee data");
            System.out.println("4.List of the employee data");
            System.out.println("5.Search the employee data");
            System.out.println("6.Group by the department");
            System.out.println("7.Sort by the name");
            System.out.println("8.Exit");
            System.out.print("enter your Choice:");
            int ch = sc.nextInt();
            switch (ch) {
                case 1:
                    System.out.println("Insert the employee details:");
                    System.out.print("Enter the id:");
                    id = sc.nextInt();
                    System.out.print("Enter the name:");
                    name = sc.next();
                    System.out.print("Enter salary:");
                    salary = sc.nextInt();
                    System.out.print("Enter the department:");
                    department = sc.next();
                    System.out.print("Enter the Address:");
                    address = sc.next();
                    hs.add(new Employee(id, name, salary, department, address));
                    System.out.println("Employee Values inserted");
                    break;
                case 2:
                    System.out.println("Delete the employee ID:");
                    id = sc.nextInt();
                    for (Employee e : hs) {
                        if (id == e.id) {
                            hs.remove(e);
                        }
                        System.out.println("Data deleted successfully");
                    }
                    System.err.println("Employee record not found");
                    break;
                case 3:
                    System.out.println("Update the Employee:");
                    id = sc.nextInt();
                    for (Employee e : hs) {
                        if (id == e.id) {
                            Employee employee = update(e, id);
                            hs.remove(e);
                            hs.add(employee);
                        }
                    }
                    System.err.println("data not found");
                    break;
                case 4:
                    System.out.println("List of the employees");
                    for (Employee e : hs) {
                        System.out.println(e.id + " " + e.getName() + " " + e.salary + " " + e.department + " " + e.address);
                    }
                    break;
                case 5:
                    System.out.println("Search Employee data:");
                    System.out.println("Search by Empid:");
                    id = sc.nextInt();
                    for (Employee e : hs) {
                        System.out.println(e.id + " " + e.getName() + " " + e.salary + " " + e.department + " " + e.address);
                    }
                    break;
                case 6:
                    System.out.println("Group by Department:");
                    Map<String, Set<Employee>> map = new HashMap<>();
                    for (Employee e : hs) {
                        if (map.containsKey(e.department)) {
                            Set<Employee> se = map.get(e.department);
                            se.add(e);
                            map.put(e.department, se);
                        } else {
                            Set<Employee> se = new HashSet<>();
                            se.add(e);
                            map.put(e.department, se);
                        }
                    }
                    System.out.print("Group by Department:"+map.entrySet());
                    break;
                case 7:
                    System.out.println("Sort the data using name:");
                    List<Employee> list = new ArrayList<Employee>(hs);
                    Collections.sort(list, new CompareName());
                    for (Employee e : list) {
                        System.out.println(e.id + " " + e.name + " " + e.salary + " " + e.department + " " + e.address);
                    }
                    break;
                case 8:
                    System.out.println("Exit the Data");
                    break;
                default:

            }
        } while (true);
    }

    private static Employee update(Employee e, int id) {
        Scanner sc = new Scanner(System.in);
        e.id = id;
        System.out.print("Enter the name of the employee to be update:");
        e.setName(sc.next());
        System.out.print("enter the salary to update:");
        e.salary = sc.nextInt();
        System.out.print("Enter the update department:");
        e.department = sc.next();
        System.out.print("Enter the Addresses to update: ");
        e.address = sc.next();
        System.out.println("Data updated:");
        return e;
    }
}

